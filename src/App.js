import React from 'react';
import logo from './logo.svg';
import './App.css';
import ToggleSwitch from './components/ToggleSwitch';

function App() {
  const [value, setValue] = useState(false);
  return (
    <div className="app">
      <ToggleSwitch
        isOn={value}
        handleToggle={() => setValue(!value)}
      />
    </div>
  );
}

export default App;
